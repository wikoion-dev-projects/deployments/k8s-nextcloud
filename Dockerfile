FROM nextcloud

COPY ./apps.sh /

ENV NEXTCLOUD_UPDATE=1

CMD ["apache2-foreground"]

ENTRYPOINT ["/apps.sh"]
