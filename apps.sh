#!/bin/sh

echo "starting nextcloud"
/entrypoint.sh $1 & sleep 10s
service apache2 stop & sleep 10s

# Required for docker deployment (won't work in kubernetes)
su -p www-data -s /bin/sh -c "/var/www/html/occ maintenance:install --admin-user=${NEXTCLOUD_ADMIN_USER} --admin-pass=${NEXTCLOUD_ADMIN_PASSWORD}"

# Required for kubernetes deployment
export OC_PASS="${NEXTCLOUD_ADMIN_PASSWORD}"
su -p www-data -s /bin/sh -c "/var/www/html/occ user:resetpassword --password-from-env ender"

su -p www-data -s /bin/sh -c "/var/www/html/occ app:enable files_external"
su -p www-data -s /bin/sh -c "/var/www/html/occ files_external:create -c bucket=k8s-nextcloud -c hostname=s3.us-west-002.backblazeb2.com -c region=us-west-002 -c use_ssl=true -c use_path_style=true -c key=${BUCKET_ID} -c secret=${BUCKET_SECRET} BackblazeB2 amazons3 amazons3::accesskey"

su -p www-data -s /bin/sh -c "/var/www/html/occ app:install notes"
sed -i "s/\$defaultFolder\ \=\ 'Notes'\;/\$defaultFolder\ \=\ 'BackblazeB2\/Notes'\;/" /var/www/html/custom_apps/notes/lib/Service/SettingsService.php

su -p www-data -s /bin/sh -c "/var/www/html/occ app:install calendar"
su -p www-data -s /bin/sh -c "/var/www/html/occ app:install contacts"
su -p www-data -s /bin/sh -c "/var/www/html/occ app:install deck"
su -p www-data -s /bin/sh -c "/var/www/html/occ app:install audioplayer"
su -p www-data -s /bin/sh -c "/var/www/html/occ app:install news"
su -p www-data -s /bin/sh -c "/var/www/html/occ app:install twofactor_u2f"

su -p www-data -s /bin/sh -c "php /var/www/html/cron.php"

apache2-foreground
